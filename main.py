from fpdf import FPDF
import csv
import random
from random import shuffle
import jaconv


# separates japanese from german
def getSeparated(data: list, language: int) -> list:
    return [x[language] for x in data]


# gets dictionary and creatues tuple from it
def getDictionary(filename) -> tuple:
    dict = getFile(filename)
    return [tuple(x) for x in dict.items()]

def createVocList(kana : int) -> dict:
    # Check for Kana-Style and get List from Dictionary-Files
    if kana == 0: vocList = list(getRandomJapanese(getDictionary('vocHira.csv'), 10))
    if kana == 1: vocList = list(getRandomJapanese(getDictionary('vocKata.csv'), 10))
    if kana >= 2: vocList = list(getRandomJapanese(getDictionary('vocHira.csv'), 10))
    # separate german from japanese
    japanese = getSeparated(vocList, 0)
    german = getSeparated(vocList, 1)
    # Change vocabulary order
    shuffle(japanese)
    shuffle(german)
    return dict(zip(japanese, german))

# opens dictionary files and returns a dictionary
def getFile(filename) -> dict:
    with open(filename, mode='r', encoding="utf-8") as infile:
        reader = csv.reader(infile, delimiter=',')
        mydict = {rows[0]: rows[1] for rows in reader}
    return mydict


# selects random words from dictionary and returns either hiragana or katakana
# with appropriate german translation
def getRandomJapanese(list: tuple, quantity: int) -> tuple:
    seen = set()
    keep = []
    i = 0
    while i < quantity:
        x = random.choice(list)
        if x[0] in seen:
            print("Duplicate found")
        else:
            seen.add(x[0])
            keep.append((x[0], x[1]))
            i += 1
    return keep

def setTable(pdfObject):
    data = [['N', 'w', 'r', 'y', 'm', 'h', 'n', 't', 's', 'k', ''],
            ['', '', '', '', '', '', '', '', '', '', 'A'],
            ['', '', '', '', '', '', '', '', '', '', 'I'],
            ['', '', '', '', '', '', '', '', '', '', 'U'],
            ['', '', '', '', '', '', '', '', '', '', 'E'],
            ['', '', '', '', '', '', '', '', '', '', 'O']
            ]
    col_width = pdfObject.w / 12
    row_height = col_width/2
    for row in data:
        for item in row:
            pdfObject.cell(col_width, row_height,
                     txt=item, border=1, ln=0, align='C')

        pdfObject.ln(row_height)


def setVocs(pdfObject,HiraganaList,KatakanaList):
    # Change font to asian
    pdfObject.add_font('fireflysung', '', 'fireflysung.ttf', uni=True)
    pdfObject.set_font('fireflysung', '', 12)
    col_width = pdfObject.w / 4.5
    row_height = col_width / 12
    # Place Hiragana & Katakana on paper
    for i in range(len(HiraganaList)):
        pdfObject.cell(col_width, row_height,txt=list(HiraganaList.keys())[i], border=0, ln=0, align='L')
        pdfObject.cell(col_width, row_height, txt=HiraganaList.get(list(HiraganaList.keys())[i]), border=0, ln=0, align='R')
        # Placeholder for Katakana
        pdfObject.cell(col_width, row_height, txt=list(KatakanaList.keys())[i], border=0, ln=0, align='L')
        pdfObject.cell(col_width, row_height, txt=KatakanaList.get(list(KatakanaList.keys())[i]), border=0, ln=0, align='R')
        pdfObject.ln(row_height)

def setDiary(pdfObject):
    # Define Start/Stop positions for lines
    xStart = 10
    yStart = 188
    xStop = 200
    yStop = 188
    col_width = pdfObject.w / 3.3
    row_height = col_width / 12
    pdfObject.ln(row_height)
    # Place Nen/Gatsu/Nichi
    pdfObject.cell(col_width, row_height, txt="\u5E74", border=0, ln=0, align='R')
    pdfObject.cell(col_width, row_height, txt=u"\u6708", border=0, ln=0, align='R')
    pdfObject.cell(col_width, row_height, txt="\u65E5", border=0, ln=0, align='R')
    # Create lines for writing
    for i in range(8):
        pdfObject.line(xStart,yStart,xStop,yStop)
        yStart += 12
        yStop += 12


pdf = FPDF()
pdf.set_font("Arial", size=12)
pdf.add_page()
font_size = 8
# get randomized dictionary
shuffledHira = createVocList(0)
shuffledKata = createVocList(1)

# Create Table-Title and Table
pdf.cell(200,12,"Hiragana",border=0,ln=1,align='C')
setTable(pdf)
pdf.cell(200, 12, "Katakana", border=0, ln=1, align='C')
setTable(pdf)
# Place Vocabulary
# TODO: Implement Katakana
setVocs(pdf, shuffledHira, shuffledKata)
# Place Diary
setDiary(pdf)
# Write PDF file
pdf.output('Worksheet.pdf')
